package br.com.xtrasense.bolt.controlls;

import br.com.xtrasense.bolt.domain.User;

/**
 * Created by natan on 14/08/17.
 */

public class SessionManager {
    private User loggedUser;

    public boolean hasLoggedSession() {
        return false;
    }

    public User getLoggedUser() {
        return loggedUser;
    }
}
