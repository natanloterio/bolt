package br.com.xtrasense.bolt.async;

import android.os.AsyncTask;

import br.com.xtrasense.bolt.controlls.MainControll;

/**
 * Created by natan on 15/08/17.
 */

public class ApplicationInitializationAsyncTask extends AsyncTask<Void,Void,MainControll> {
    private IApplicationInitializationAsyncTask callback;

    public ApplicationInitializationAsyncTask(IApplicationInitializationAsyncTask callback) {
        this.callback = callback;
    }

    @Override
    protected MainControll doInBackground(Void... params) {
        return new MainControll();
    }

    public interface IApplicationInitializationAsyncTask{

        void afterInitMainControll(MainControll mainControll);
    }

    @Override
    protected void onPostExecute(MainControll mainControll) {
        super.onPostExecute(mainControll);
        callback.afterInitMainControll(mainControll);
    }
}
