package br.com.xtrasense.bolt.views.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Random;

import br.com.xtrasense.bolt.R;

public class VerticalFragment extends Fragment {

    public VerticalFragment() {
    }

    public static Fragment newInstance(String title, int position) {
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putInt("position", position);
        VerticalFragment fragment = new VerticalFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.vertical_fragment, container, false);
        //view.findViewById(R.id.fragment_content_conatiner).setBackgroundColor(getRandonCollor());
        String title = getArguments().getString("title");
        int verticalPage = getArguments().getInt("position");
        ((TextView)view.findViewById(R.id.fragment_content_textview)).setText(title+":"+verticalPage);
        return view;
    }


    public String getTitle() {
        return getArguments().getString("title");
    }

    public int getRandonCollor() {
        // create random object - reuse this as often as possible
        Random random = new Random();

        // create a big random number - maximum is ffffff (hex) = 16777215 (dez)
        int nextInt = random.nextInt(11447725);

        // format it as hexadecimal string (with hashtag and leading zeros)
        String colorCode = String.format("#%06x", nextInt);

        // print it
        return Color.parseColor(colorCode);
    }
}
