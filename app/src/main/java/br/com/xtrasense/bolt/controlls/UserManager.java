package br.com.xtrasense.bolt.controlls;

import br.com.xtrasense.bolt.controlls.exceptions.LoginException;
import br.com.xtrasense.bolt.domain.UserSession;
import br.com.xtrasense.bolt.domain.User;

/**
 * Created by natan on 14/08/17.
 */

public abstract class UserManager {

    public abstract void createUser(User user);
    public abstract void upateUser(User user);
    public abstract UserSession loginUser(User user) throws LoginException;

}
