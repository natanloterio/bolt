package br.com.xtrasense.bolt.views.components;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import br.com.xtrasense.transforms.DefaultTransformer;

/**
 * Created by natan.loterio on 16/08/2017.
 */

public class HorizontalViewPager extends ViewPager {
    public HorizontalViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        //setPageTransformer(false, new DefaultTransformer());
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        boolean intercept = super.onInterceptTouchEvent(event);
        Log.e("BOLT","HorizontalViewPager.onInterceptTouchEvent");
        return intercept;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        Log.e("BOLT","HorizontalViewPager.onTouchEvent");
        return super.onTouchEvent(ev);
    }

}
