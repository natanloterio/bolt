package br.com.xtrasense.bolt.views.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import br.com.xtrasense.bolt.controlls.IMainControll;
import br.com.xtrasense.bolt.views.fragments.HorizontalFragment;

/**
 * Created by natan on 14/08/17.
 */

public class VerticalViewPagerAdapter extends FragmentPagerAdapter {

    private IMainControll mainControll;

    public VerticalViewPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }


    @Override
    public int getCount() {
        return getMainControll().getHorizontalScreensCount();

    }

    @Override
    public Fragment getItem(int position) {
        return new HorizontalFragment();
    }

    public IMainControll getMainControll() {
        return mainControll;
    }

    public void setMainControll(IMainControll mainControll) {
        this.mainControll = mainControll;
    }
}
