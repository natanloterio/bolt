package br.com.xtrasense.bolt.views.components;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import br.com.xtrasense.transforms.DefaultTransformer;

/**
 * Copyright (C) 2015 Kaelaela
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

public class VerticalViewPager extends ViewPager {

    private boolean movimentoVertical;

    public VerticalViewPager(Context context) {
        this(context, null);
    }

    public VerticalViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        setPageTransformer(false, new DefaultTransformer());
    }

    private MotionEvent swapTouchEvent(MotionEvent event) {
        float width = getWidth();
        float height = getHeight();

        float swappedX = (event.getY() / height) * width;
        float swappedY = (event.getX() / width) * height;

        event.setLocation(swappedX, swappedY);

        return event;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        boolean intercept = super.onInterceptTouchEvent(swapTouchEvent(event));
        //If not intercept, touch event should not be swapped.
        swapTouchEvent(event);
        Log.e("BOLT","VerticalViewPager.onInterceptTouchEvent");
        return shouldIntercept(event);
    }

    private boolean shouldIntercept(MotionEvent event) {
        int totalHistorico = event.getHistorySize();
        if(totalHistorico >= 2){
            float axis_x = event.getAxisValue(MotionEvent.AXIS_X);
            float axis_y = event.getAxisValue(MotionEvent.AXIS_Y);
            float x1 = event.getHistoricalX(1);
            float y1 = event.getHistoricalY(1);
            float x2,y2,dx, dy;
            x2 = event.getX();
            y2 = event.getY();
            dx = x2-x1;
            dy = y2-y1;
            String direction = "";
            // Use dx and dy to determine the direction
            if(Math.abs(dx) > Math.abs(dy)) {
                movimentoVertical = false;
                if(dx>0)
                    direction = "right";
                else
                    direction = "left";
            } else {
                movimentoVertical = true;
                if(dy>0)
                    direction = "down";
                else
                    direction = "up";
            }
            Log.e("BOLT","Direction:"+direction);
            return movimentoVertical;
        }
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        Log.e("BOLT","VerticalViewPager.onTouchEvent");
        return super.onTouchEvent(swapTouchEvent(ev));
    }

}
