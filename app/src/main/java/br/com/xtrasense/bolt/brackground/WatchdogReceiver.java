package br.com.xtrasense.bolt.brackground;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import static br.com.xtrasense.bolt.brackground.WatchDog.isRunning;

public class WatchdogReceiver extends BroadcastReceiver {

    private Class<?> serviceClass = LocationService.class;

    @Override
        public void onReceive(Context context, Intent intent) {
            if(isRunning(context)){
                makeSureServiceIsRunning(context);
            }
        }

        private void makeSureServiceIsRunning(Context context) {
            context.startService(new Intent(context,serviceClass));
        }
    }