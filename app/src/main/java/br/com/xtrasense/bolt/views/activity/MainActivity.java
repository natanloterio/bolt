package br.com.xtrasense.bolt.views.activity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;

import br.com.xtrasense.bolt.R;
import br.com.xtrasense.bolt.async.ApplicationInitializationAsyncTask;
import br.com.xtrasense.bolt.brackground.WatchDog;
import br.com.xtrasense.bolt.controlls.IMainControll;
import br.com.xtrasense.bolt.controlls.MainControll;
import br.com.xtrasense.bolt.views.IMainView;
import br.com.xtrasense.bolt.views.adapters.VerticalViewPagerAdapter;
import br.com.xtrasense.bolt.views.components.VerticalViewPager;

public class MainActivity extends AppCompatActivity implements ApplicationInitializationAsyncTask.IApplicationInitializationAsyncTask, IMainView {


    private VerticalViewPager viewPager;
    private View view;
    private IMainControll mMainControll;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = LayoutInflater.from(this).inflate(R.layout.main_activity, null);
        setView(view);
        setContentView(view);
        initialization();
        WatchDog.startWatching(this);
    }

    private void initialization() {
        (new ApplicationInitializationAsyncTask(this)).execute();
    }

    public void setupAdapter() {
        VerticalViewPagerAdapter verticalViewPagerAdapter = new VerticalViewPagerAdapter(getSupportFragmentManager());
        verticalViewPagerAdapter.setMainControll(getMainControll());
        viewPager.setAdapter(verticalViewPagerAdapter);
    }

    @Override
    public void afterInitMainControll(MainControll mainControll) {
        setmMainControll(mainControll);

        viewPager = (VerticalViewPager) getView().findViewById(R.id.mainactivity_vertical_viewpager);

        if (viewPager != null) {
            setupAdapter();
        }
    }

    public void setmMainControll(IMainControll mMainControll) {
        this.mMainControll = mMainControll;
        mMainControll.setMainView(this);
    }

    public void setView(View view) {
        this.view = view;
    }

    public View getView() {
        return view;
    }


    public IMainControll getMainControll() {
        return mMainControll;
    }
}
