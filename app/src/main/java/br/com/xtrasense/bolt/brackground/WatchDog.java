package br.com.xtrasense.bolt.brackground;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;


import java.util.Calendar;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by natan on 26/08/17.
 */

public class WatchDog {

    private static final int REQUEST_CODE = 666;
    private static final int WATCHDOG_CHECK_INTERVAL = 2;
    private static String WATCHDOG_RUNNING = "WATCHDOG_RUNNING";


    public static void startWatching(Context context) {
        setRunning(context,true);
        scheduleCheck(context);
    }

    public static void setRunning(Context context, boolean running) {
        SharedPreferences sp = context.getSharedPreferences(context.getPackageName(), MODE_PRIVATE);
        sp.edit().putBoolean(WATCHDOG_RUNNING,running).apply();
    }

    public static boolean isRunning(Context context) {
        SharedPreferences sp = context.getSharedPreferences(context.getPackageName(), MODE_PRIVATE);
        return sp.getBoolean(WATCHDOG_RUNNING,false);
    }

    private static void scheduleCheck(Context context){
        // get a Calendar object with current time
        Calendar calendar = java.util.Calendar.getInstance();
        // add 30 seconds to the calendar object

        calendar.add(Calendar.SECOND, WATCHDOG_CHECK_INTERVAL);
        Intent intent = new Intent(context, WatchdogReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Get the AlarmManager service
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender);
    }



}
