package br.com.xtrasense.bolt.views.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.Random;

import br.com.xtrasense.bolt.views.adapters.HorizontalViewPagerAdapter;
import br.com.xtrasense.bolt.R;
import br.com.xtrasense.bolt.views.components.HorizontalViewPager;

/**
 * Created by natan on 15/08/17.
 */

public class HorizontalFragment extends Fragment {
    private static int count;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.horizontal_fragment, container, false);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.fragment_base_container);
        //linearLayout.setBackgroundColor(getRandonCollor());
        count++;
        //((TextView)view.findViewById(R.id.fragment_base_textview)).setText("Count:"+container);
        initViewPager(view);
        return view;
    }
    private void initViewPager(View view) {
        HorizontalViewPager viewPager = (HorizontalViewPager) view.findViewById(R.id.vertical_horizontal_viewpager);
        //viewPager.setPageTransformer(false, new ZoomOutTransformer());
        //viewPager.setPageTransformer(true, new StackTransformer());
        String title = "VerticalFragment:"+count;
        viewPager.setAdapter(new HorizontalViewPagerAdapter.Holder(getChildFragmentManager())
                .add(VerticalFragment.newInstance(title, 1))
                .add(VerticalFragment.newInstance(title, 2))
                .add(VerticalFragment.newInstance(title, 3))
                .add(VerticalFragment.newInstance(title, 4))
                .set());
        //If you setting other scroll mode, the scrolled fade is shown from either side of display.
        viewPager.setOverScrollMode(View.OVER_SCROLL_NEVER);
    }


    public int getRandonCollor() {
        // create random object - reuse this as often as possible
        Random random = new Random();

        // create a big random number - maximum is ffffff (hex) = 16777215 (dez)
        int nextInt = random.nextInt(256*256*256);

        // format it as hexadecimal string (with hashtag and leading zeros)
        String colorCode = String.format("#%06x", nextInt);

        // print it
        return Color.parseColor(colorCode);
    }
}
