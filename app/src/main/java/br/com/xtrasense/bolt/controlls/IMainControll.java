package br.com.xtrasense.bolt.controlls;

import br.com.xtrasense.bolt.views.IMainView;
import br.com.xtrasense.bolt.views.activity.MainActivity;

/**
 * Created by natan on 15/08/17.
 */
public interface IMainControll {
    int getHorizontalScreensCount();

    void setMainView(IMainView mainView);
}
