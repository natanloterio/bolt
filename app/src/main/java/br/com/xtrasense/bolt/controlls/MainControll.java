package br.com.xtrasense.bolt.controlls;

import br.com.xtrasense.bolt.views.IMainView;

/**
 * Created by natan on 14/08/17.
 */

public class MainControll implements IMainControll {

    private SessionManager sessionManager;
    private ScreenManager screenManager;
    private IMainView viewCommandExecutor;

    public MainControll(){

    }

    public ScreenManager getScreenManager() {
        return screenManager;
    }

    public SessionManager getSessionManager() {
        return sessionManager;
    }

    private void initialization(){
        // se está logado, abre tela inicial
        if(getSessionManager().hasLoggedSession()){
            getScreenManager().openLoggedScreen(getSessionManager().getLoggedUser());
        }else {
            getScreenManager().openMainScreen();
        }
    }


    public void setMainView(IMainView viewCommandExecutor) {
        this.viewCommandExecutor = viewCommandExecutor;
    }

    @Override
    public int getHorizontalScreensCount() {

        return 5;
    }
}
