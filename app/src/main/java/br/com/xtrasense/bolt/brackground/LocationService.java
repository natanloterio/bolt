package br.com.xtrasense.bolt.brackground;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by natan on 26/08/17.
 */

public class LocationService extends Service {

    private static final long INTERVALO = 2000;
    private static final String TAG = "LOCATION_SERVICE";
    private Handler handler;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG,"onCreate");

        WatchDog.startWatching(this);
        startLocatoinServices();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG,"onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG,"onDestroy");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.i(TAG,"onTaskRemoved");
    }

    private void initHandler() {
        this.handler = new Handler();
    }

    private void startLocatoinServices() {
        initHandler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(LocationService.this, "Posicao recebida", Toast.LENGTH_SHORT).show();

                handler.postDelayed(this,INTERVALO);
            }
        },INTERVALO);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
